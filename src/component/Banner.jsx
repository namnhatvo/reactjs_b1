import React from "react";

export const Banner = () => {
  return (
    <div
      style={{ height: "400px", borderRadius: "8px", width: "75%" }}
      className="container mt-5 text-center bg-light"
    >
      <h1
        style={{ paddingTop: "100px", fontSize: "50px" }}
        className="font-weight-bold"
      >
        A warm welcome!
      </h1>
      <p style={{ fontSize: "25px"}}>
        Bootstrap utility classes are used to create this jumbotron since the
        old component has been removed from the framework. Why create custom CSS
        when you can use utilities?
      </p>
      <button
        style={{ fontSize: "20px", borderRadius: "8px" }}
        className="btn btn-primary"
      >
        Call to action
      </button>
    </div>
  );
};
