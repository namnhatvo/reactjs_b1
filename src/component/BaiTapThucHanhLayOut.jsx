
import React from 'react'
import Header from './Header'
import Body from './Body'
import Footer from './Footer'

const BaiTapThucHanhLayOut = () => {
  return (
    <div>
        <Header/>
        <Body/>
        <Footer/>
    </div>
  )
}

export default BaiTapThucHanhLayOut