import React from "react";
import "../style.css";

const Item = () => {
  return (
    <div
      style={{
        display: "grid",
        columnGap: "10px",
        gridTemplateColumns: " auto auto auto",
      }}
      className="container"
    >
      <div
        style={{
          marginLeft: "79px",
          marginTop: "70px",
          borderRadius: "8px",
          width: "69%",
          marginRight: "-34px",
        }}
        className="text-center bg-light p-2"
      >
        <div>
          <div
            style={{
              width: "70px",
              height: "55px",
              margin: "-30px 0px 30px 120px",
              borderRadius: "8px",
            }}
            className="bg-primary text-white "
          >
            <i class="bi bi-collection fs-2 "></i>
          </div>
          <h4 className="font-weight-bold w-100">Fresh new layout</h4>
          <p>
            With Bootstrap 5, we've created a fresh new layout for this
            template!
          </p>
        </div>
      </div>
      <div
        style={{
          marginLeft: "13px",
          marginTop: "70px",
          borderRadius: "8px",
          width: "82%",
        }}
        className="text-center bg-light p-2"
      >
        <div>
          <div
            style={{
              width: "70px",
              height: "55px",
              margin: "-30px 0px 30px 120px",
              borderRadius: "8px",
            }}
            className="bg-primary text-white "
          >
            <i class="bi bi-cloud-download fs-2"></i>
          </div>
          <h4 className="font-weight-bold w-100">Free to download</h4>
          <p>
          As always, Start Bootstrap has a powerful collectin of free templates.
          </p>
        </div>
      </div>
      <div
        style={{
          marginLeft: "20px",
          marginTop: "70px",
          borderRadius: "8px",
          width: "76%",
        }}
        className="text-center bg-light p-2"
      >
        <div>
          <div
            style={{
              width: "70px",
              height: "55px",
              margin: "-30px 0px 30px 120px",
              borderRadius: "8px",
            }}
            className="bg-primary text-white "
          >
            <i class="bi bi-card-heading fs-2"></i>
          </div>
          <h4 className="font-weight-bold w-100">Jumbotron hero header</h4>
          <p>
          The heroic part of this template is the jumbotron hero header!
          </p>
        </div>
      </div>
      <div
        style={{
          marginLeft: "80px",
          marginTop: "85px",
          borderRadius: "8px",
          width: "70%",
        }}
        className="text-center bg-light p-2"
      >
        <div>
          <div
            style={{
              width: "70px",
              height: "55px",
              margin: "-30px 0px 30px 120px",
              borderRadius: "8px",
            }}
            className="bg-primary text-white "
          >
            <i class="bi bi-bootstrap fs-2"></i>
          </div>
          <h4 className="font-weight-bold w-100">Feature boxes</h4>
          <p>
          We've created some custom feature boxes using Bootstrap icons!
          </p>
        </div>
      </div>
      <div
        style={{
          marginLeft: "13px",
          marginTop: "85px",
          borderRadius: "8px",
          width: "82%",
        }}
        className="text-center bg-light p-2"
      >
        <div>
          <div
            style={{
              width: "70px",
              height: "55px",
              margin: "-30px 0px 30px 120px",
              borderRadius: "8px",
            }}
            className="bg-primary text-white "
          >
            <i class="bi bi-code fs-2 "></i>
          </div>
          <h4 className="font-weight-bold w-100">Simple clean code</h4>
          <p>
          We keep our dependencies up to date and squash bugs as they come!
          </p>
        </div>
      </div>
      <div
        style={{
          marginLeft: "20px",
          marginTop: "85px",
          borderRadius: "8px",
          width: "76%",
        }}
        className="text-center bg-light p-2"
      >
        <div>
          <div
            style={{
              width: "70px",
              height: "55px",
              margin: "-30px 0px 30px 120px",
              borderRadius: "8px",
            }}
            className="bg-primary text-white "
          >
            <i class="bi bi-patch-check fs-2 "></i>
          </div>
          <h4 className="font-weight-bold w-100">A name you trust</h4>
          <p>
          Start Bootstrap has been the leader in free Bootstrap templates since 2013!
          </p>
        </div>
      </div>
    </div>
  );
};

export default Item;
