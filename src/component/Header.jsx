
import React from 'react'

const Header = () => {
  return (
    <div className=' bg-dark d-flex justify-content-between align-items-center p-3'>
        <div className='title '>
            <h5 style={{marginLeft: "170px"}} className='text-white'>Start Bootstrap</h5>
        </div>
        <div style={{marginRight: "170px"}} className='link text-white'>
            <a className='mr-3'>Home</a>
            <a className='mr-3'>About</a>
            <a className='mr-3'>Contact</a>
        </div>

    </div>
  )
}

export default Header