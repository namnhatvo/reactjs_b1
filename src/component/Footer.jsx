
import React from 'react'

const Footer = () => {
  return (
    <div
     className=' mt-5 bg-dark p-5'>
        <p class="m-0 text-center text-white">Copyright © Your Website 2023</p>
     </div>
  )
}

export default Footer